<?php

declare(strict_types=1);

namespace Equidea\Database;

use PDO;
use PDOException;
use PDOStatement;

/**
 * Database wrapper class for enabling method chaining.
 */
class Database
{
    private PDO $pdo;
    private PDOStatement $stmt;
    
    public function __construct(array $config)
    {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $dsn = 'mysql:host=' . $config['hostname'] . ';';
        $dsn .= 'dbname=' . $config['database'] . ';';
        $dsn .= 'charset=' . $config['charset'] . ';';

        try {
            $this->pdo = new PDO(
                $dsn,
                $config['username'],
                $config['password'],
                $options
            );
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    /**
     * Creates a new prepared statement
     */
    public function query(string $query) : Database
    {
        $this->stmt = $this->pdo->prepare($query);
        return $this;
    }

    /**
     * Binds parameters to a prepared statement.
     */
    public function bind(mixed $id, mixed $value, int $type = null) : Database
    {
        if(is_null($type))
        {
            $type = match (true) {
                is_null($value) => PDO::PARAM_NULL,
                is_int($value) => PDO::PARAM_INT,
                is_bool($value) => PDO::PARAM_BOOL,
                default => PDO::PARAM_STR,
            };
        }

        $this->stmt->bindValue($id, $value, $type);
        return $this;
    }

    /**
     * Executes a prepared statement.
     */
    public function execute() : bool {
        return $this->stmt->execute();
    }

    /**
     * Returns all result rows.
     */
    public function fetchAll(int $mode = PDO::FETCH_DEFAULT) : array
    {
        $this->execute();
        return $this->stmt->fetchAll($mode);
    }

    /**
     * Selects one result row.
     */
    public function fetch(int $mode = PDO::FETCH_DEFAULT) : array
    {
        $this->execute();
        return $this->stmt->fetch($mode);
    }

    /**
     * Selects one result column.
     */
    public function fetchColumn(int $position = 0) : mixed
    {
        $this->execute();
        return $this->stmt->fetchColumn($position);
    }

    /**
     * Get last inserted id.
     */
    public function getLastInsertId() : int {
        return (int) $this->pdo->lastInsertId();
    }
}
