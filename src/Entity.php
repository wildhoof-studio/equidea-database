<?php

declare(strict_types=1);

namespace Equidea\Database;

use function array_filter;
use function array_merge;
use function in_array;

use const ARRAY_FILTER_USE_KEY;

/**
 * An object representation of a database table row.
 */
abstract class Entity
{
    protected string $tableName;
    protected string $primaryKey;

    protected array $columns = [];
    protected array $changedColumns = [];

    public function __construct(
        protected Database $database,
        protected Query $query
    ) {}

    /**
     * Mark column as changed internally.
     */
    protected function addChangedColumn(string $column) : void {
        $this->changedColumns[] = $column;
    }

    /**
     * Get a list of all the changed columns.
     */
    protected function getChangedColumns() : array
    {
        $keys = $this->changedColumns;

        return array_filter($this->columns, function($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Reset the changed columns to the default state.
     */
    protected function resetChangedColumns() : void {
        $this->changedColumns = [];
    }

    /**
     * Set primary key.
     */
    public function setPrimaryKey(int $value) : void {
        $this->columns[$this->primaryKey] = $value;
    }

    /**
     * Sets an integer value.
     */
    public function setIntValue(string $column, int $value) : void
    {
        $this->columns[$column] = $value;
        $this->addChangedColumn($column);
    }

    /**
     * Retrieves an integer value.
     */
    public function getIntValue(string $name) : ?int {
        return (int) $this->columns[$name];
    }

    /**
     * Sets a string value.
     */
    public function setStringValue(string $column, string $value) : void
    {
        $this->columns[$column] = $value;
        $this->addChangedColumn($column);
    }

    /**
     * Retrieves a string value.
     */
    public function getStringValue(string $name) : ?string {
        return (string) $this->columns[$name];
    }

    /**
     * Saves the entity to the database and return the new entry id.
     */
    public function save() : int
    {
        // Build sql
        $sql = $this->query->insert()
            ->table($this->tableName)
            ->columns(...$this->changedColumns);

        $this->database->query((string) $sql);

        // Bind all parameters
        foreach ($this->getChangedColumns() as $key => $value) {
            $this->database->bind($key, $value);
        }

        // Save to database.
        $this->database->execute();
        $this->resetChangedColumns();

        // Save the new index key
        $newId = $this->database->getLastInsertId();
        $this->columns[$this->primaryKey] = $newId;
        return $newId;
    }

    /**
     * Retrieve parts of a dataset from the database by primary key.
     */
    public function retrieve(string ...$columns) : void {
        $this->retrieveBy($this->primaryKey, ...$columns);
    }

    /**
     * Retrieve parts of a dataset from the database by primary key.
     */
    public function retrieveBy(string $by, string ...$columns) : void
    {
        // Build the query string
        $sql = $this->query->select()
            ->table($this->tableName)
            ->columns(...$columns)
            ->where($by)
            ->limit(1);

        // Get results from the database.
        $result = $this->database->query((string) $sql)
            ->bind(
                $by,
                $this->columns[$by]
            )->fetch();

        $this->columns = array_merge($this->columns, $result);
    }

    /**
     * Updates the database entry by using its primary key as identifier.
     */
    public function update() : void
    {
        // Build sql
        $sql = $this->query->update()
            ->table($this->tableName)
            ->set(...$this->changedColumns)
            ->where($this->primaryKey);

        $this->database->query((string) $sql);

        // Bind all parameters
        foreach ($this->getChangedColumns() as $key => $value) {
            $this->database->bind($key, $value);
        }

        // Bind the primary key as identifier and execute.
        $this->database->bind(
            $this->primaryKey,
            $this->columns[$this->primaryKey]
        )->execute();

        // Reset changed columns to signify class is "up to date".
        $this->resetChangedColumns();
    }
}
