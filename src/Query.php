<?php

declare(strict_types=1);

namespace Equidea\Database;

use Equidea\Database\Query\Select;
use Equidea\Database\Query\Count;
use Equidea\Database\Query\Insert;
use Equidea\Database\Query\Update;
use Equidea\Database\Query\Delete;

/**
 * Class for building the query string.
 */
class Query
{
    public const PLACEHOLDER_PREFIX = ':';
    public const ESCAPE_STRING = '`';

    public const LIMIT_SCHEMA = ' LIMIT %u, %u';

    public const WHERE_SCHEMA = ' WHERE %s';
    public const WHERE_IS = '`%s` = :%s';
    public const WHERE_NOT = '`%s` != :%s';
    public const WHERE_SMALLER = '`%s` < :%s';
    public const WHERE_GREATER = '`%s` > :%s';

    public const COMBINE_COMMA = ', ';
    public const COMBINE_AND = ' AND ';

    /**
     * Creates a new select statement.
     */
    public function select() : Select {
        return new Select();
    }

    /**
     * Create a new count select statement.
     */
    public function count() : Count {
        return new Count();
    }

    /**
     * Creates a new insert statement.
     */
    public function insert() : Insert {
        return new Insert();
    }

    /**
     * Creates a new update statement.
     */
    public function update() : Update {
        return new Update();
    }

    /**
     * Creates a new delete statement.
     */
    public function delete() : Delete {
        return new Delete();
    }
}
