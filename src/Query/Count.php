<?php

declare(strict_types=1);

namespace Equidea\Database\Query;

use Equidea\Database\Query\Syntax\Limit;
use Equidea\Database\Query\Syntax\Where;

use function sprintf;

/**
 * Class for building a query string that counts rows.
 */
class Count extends AbstractQuery
{
    use Where;
    use Limit;

    private const SCHEMA = 'SELECT COUNT(1) AS result FROM `%s`';

    /**
     * Get query object as a string
     */
    public function getQueryString() : string
    {
        return sprintf(
            self::SCHEMA,
            $this->table
        ) . $this->getWhere() . $this->getLimit();
    }
}
