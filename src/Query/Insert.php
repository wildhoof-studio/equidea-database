<?php

declare(strict_types=1);

namespace Equidea\Database\Query;

use Equidea\Database\Query;
use Equidea\Database\Query\Syntax\Columns;

use function implode;
use function sprintf;

/**
 * Class for building an insert query string.
 */
class Insert extends AbstractQuery
{
    use Columns;

    private const SCHEMA = 'INSERT INTO `%s` (%s) VALUES (%s)';

    /**
     * Get values as a chained string.
     */
    protected function getValues() : string
    {
        $placeholders = [];

        // Create a list of placeholders
        foreach ($this->columns as $column) {
            $placeholders[] = Query::PLACEHOLDER_PREFIX . $column;
        }

        return implode(Query::COMBINE_COMMA, $placeholders);
    }

    /**
     * Get query object as a string
     */
    public function getQueryString() : string
    {
        return sprintf(
            self::SCHEMA,
            $this->table,
            $this->getColumns(),
            $this->getValues()
        );
    }
}
