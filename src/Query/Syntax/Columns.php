<?php

declare(strict_types=1);

namespace Equidea\Database\Query\Syntax;

use Equidea\Database\Query;

use function implode;

/**
 * Trait for adding columns to a query.
 */
trait Columns
{
    protected array $columns = [];

    /**
     * Add the column names to the object.
     */
    public function columns(string ...$columns) : self
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * Get columns to insert as a chained string.
     */
    protected function getColumns() : string
    {
        $columns = [];

        // Create a list of placeholders
        foreach ($this->columns as $column) {
            $columns[] = Query::ESCAPE_STRING . $column . Query::ESCAPE_STRING;
        }

        return implode(', ',$columns);
    }
}
