<?php

declare(strict_types=1);

namespace Equidea\Database\Query\Syntax;

use Equidea\Database\Query;

use function implode;
use function sprintf;

/**
 * Trait for adding where conditions to a query.
 */
trait Where
{
    protected array $conditions = [];

    /**
     * Add column and placeholder of an equal condition to where.
     */
    public function where(string $column, string $placeholder = null) : self
    {
        $this->conditions[] = sprintf(
            Query::WHERE_IS,
            $column,
            $placeholder ?? $column
        );

        return $this;
    }

    /**
     * Add column and placeholder for a not like condition to where.
     */
    public function whereNot(string $column, string $placeholder = null) : self
    {
        $this->conditions[] = sprintf(
            Query::WHERE_NOT,
            $column,
            $placeholder ?? $column
        );

        return $this;
    }

    /**
     * Add a column and placeholder for a greater than condition to where.
     */
    public function whereGreater(
        string $column,
        string $placeholder = null
    ) : self
    {
        $this->conditions[] = sprintf(
            Query::WHERE_GREATER,
            $column,
            $placeholder ?? $column
        );

        return $this;
    }

    /**
     * Add a column and placeholder for a smaller than condition to where.
     */
    public function whereSmaller(
        string $column,
        string $placeholder = null
    ) : self
    {
        $this->conditions[] = sprintf(
            Query::WHERE_SMALLER,
            $column,
            $placeholder ?? $column
        );

        return $this;
    }

    /**
     * Get entire where string part.
     */
    protected function getWhere() : string
    {
        // If no where is present, just return empty string.
        if (empty($this->conditions)) {
            return '';
        }

        // Otherwise return entire where fragment.
        return sprintf(
            Query::WHERE_SCHEMA,
            implode(Query::COMBINE_AND, $this->conditions)
        );
    }
}
