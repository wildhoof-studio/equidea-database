<?php

declare(strict_types=1);

namespace Equidea\Database\Query;

use Equidea\Database\Query;
use Equidea\Database\Query\Syntax\Where;

use function implode;
use function sprintf;

/**
 * Class for building an update query string.
 */
class Update extends AbstractQuery
{
    use Where;

    private const SCHEMA = 'UPDATE `%s` SET %s';

    private array $updates = [];

    /**
     * Adds the values to update
     */
    public function set(string ...$columns) : Update
    {
        $this->updates = $columns;
        return $this;
    }

    /**
     * Get the fragment to be inserted after SET.
     */
    protected function getUpdates() : string
    {
        $updates = [];

        foreach ($this->updates as $column)
        {
            $updates[] = sprintf(
                Query::WHERE_IS,
                $column,
                $column
            );
        }

        return implode(Query::COMBINE_COMMA, $updates);
    }

    /**
     * Get query object as a string
     */
    public function getQueryString() : string
    {
        return sprintf(
            self::SCHEMA,
            $this->table,
            $this->getUpdates()
        ) . $this->getWhere();
    }
}
