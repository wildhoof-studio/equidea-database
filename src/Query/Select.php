<?php

declare(strict_types=1);

namespace Equidea\Database\Query;

use Equidea\Database\Query\Syntax\Columns;
use Equidea\Database\Query\Syntax\Limit;
use Equidea\Database\Query\Syntax\Where;

use function sprintf;

/**
 * Class for building a select query string.
 */
class Select extends AbstractQuery
{
    use Columns;
    use Where;
    use Limit;

    private const SCHEMA = 'SELECT %s FROM `%s`';

    /**
     * Get query object as a string
     */
    public function getQueryString() : string
    {
        return sprintf(
            self::SCHEMA,
            $this->getColumns(),
            $this->table
        ) . $this->getWhere() . $this->getLimit();
    }
}
