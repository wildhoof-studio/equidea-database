
## Equidea Database

Database Package with
* Query Builder with
    * Select
    * Insert
    * Update
    * Delete
* Database Wrapper
* Very simple ORM

as it is used in Equidea.

### Requirements

* PHP 8.1
* PDO
